import glob
from moviepy.editor import *
import os
import datetime

'''
REQUIRED PYTHON LIBRARY: moviepy

HOW TO INSTALL LIBRARY: 
pip install moviepy

Intended Usage: This movie tool is only capable of generating movies from PICTURES. 

User Defined Inputs: 
1. Path to pictures (provide absolute path)
2. Desired length of movie (seconds)
3. Name of movie
Program Defined input
4. Location to put the movie - will take the directory of where the pictures are, create a new directory inside called
    "movies_#hour_#minute' where #hour is the current hour of the day (military) and #minute is current minute.
    EX. movies_15_45
    
Outputs:
1. Creates an mp4 video file of all the pictures provided by user

How to Run:

From the terminal enter the following:
python3 /home/YOUR_USER_NAME/Pegasus/tools/movie_maker/picture_to_movie.py

you will then be prompted with 3 questions (the 3 user defined inputs described above) 
EX. 
Enter Where Pictures are Located (paste the absolute path here): /home/dnightingale/PegasusVnV/cases/Hardy0.3MPa/pictures
Enter Length of Movie (seconds): 10
Enter Name of Movie: test_movie

After a few moments, the video will render and be available inside the user provided picture directory 
EX. movie path location
/home/dnightingale/PegasusVnV/cases/Hardy0.3MPa/pictures/movies_15_45

'''


def movie_maker(picture_location, movie_length, movie_name, movie_destination):

    # Setting frame rate
    fps = 30

    # Get all the pictures from specified directory
    file_list = glob.glob(f'{picture_location}/*.png')
    number_of_pics = float(len(file_list))

    image_duration = float(movie_length) / number_of_pics

    # Sort photos by time stamp
    file_list.sort(key=os.path.getmtime)

    # Setting duration for each picture
    clips = [ImageClip(m).set_duration(image_duration)
             for m in file_list]

    # Concatenate all pictures together
    concat_clip = concatenate_videoclips(clips, method="compose")

    # Create final video and specify name and location
    concat_clip.write_videofile(f'{movie_destination}/{movie_name}.mp4', fps=fps)


if __name__ == '__main__':
    picture_directory = input("Enter Where Pictures are Located (paste the absolute path here): ")
    movie_length = input("Enter Length of Movie (seconds): ")
    movie_name = input("Enter Name of Movie: ")

    current_time = datetime.datetime.now()
    movie_directory = f'movies_{current_time.hour}_{current_time.minute}'
    path_to_movie = os.path.join(picture_directory, movie_directory)
    os.mkdir(path_to_movie)
    movie_maker(picture_directory, movie_length, movie_name, path_to_movie)
