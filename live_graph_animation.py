from matplotlib import pyplot as plt
from matplotlib import animation
import openpyxl
import os
import datetime

'''
REQUIRED PYTHON LIBRARY: openpyxl

HOW TO INSTALL LIBRARY: 
pip install openpyxl

Intended Usage: This movie tool is only capable of generating movies from GRAPHICAL DATA in excel workbooks (.xlsx). 
The Data must be separated into two columns for this script to work. (DOES NOT WORK WITH GTD FILES YET)
EX.
TIME  |  POWER
------|-------
1     |  10
2     |  10

User Defined Inputs: 
1. Excel workbook location (provide absolute path)
2. Excel workbook name
3. Name of sheet in the workbook to make a movie from
Program Defined input
4. Location to put the movie - will take the directory of where the data is, create a new directory inside called
    "movies_#hour_#minute' where #hour is the current hour of the day (military) and #minute is current minute.
    EX. movies_15_45

Outputs:
1. Creates an mp4 video file of the data over time

How to Run:

From the terminal enter the following:
python3 /home/YOUR_USER_NAME/Pegasus/tools/movie_maker/live_graph_annimation.py

Then 3 questions will be promted (the 3 user defined inputs described above) 
EX. 
Enter where excel workbook is located (paste the absolute path here): /home/dnightingale/PegasusVnV/cases/dn_testing/IFA_439_BC/mine/
Enter workbook name:  rod_data.xlsx
Enter sheet name in workbook:  power_history

If the data was found the terminal will print:
Creating Movie, please wait...

After a few moments (may take longer with more data, be patient), the video will render and be available inside the user provided picture directory 
EX. movie path location
/home/dnightingale/PegasusVnV/cases/dn_testing/IFA_439_BC/mine//movies_15_45

If the data was NOT found the terminal will print:
Could not find file. Check that the provided path and file name are correct.

'''


class GraphAnnimation:
    def __init__(self):
        self.fig_and_ax = plt.subplots(1, 1, figsize=(6, 6))
        self.fig = self.fig_and_ax[0]
        self.ax = self.fig_and_ax[1]
        self.x_data = []
        self.y_data = []
        self.file_location = '/home/'
        self.data_file_name = 'example_data.xlsx'
        self.sheet_name = 'power_history'
        self.x_lim = [0, 1]
        self.y_lim = [0, 1]
        self.x_label = 'x label'
        self.y_label = 'y label'
        self.title = 'title'
        self.ax.set_xlabel(self.x_label)
        self.ax.set_ylabel(self.y_label)
        self.ax.set_title(self.title)

    def animate(self, i):
        self.get_data()
        self.ax.cla()  # clear the previous image
        self.ax.plot(self.x_data[:i], self.y_data[:i])  # plot the line
        self.ax.set_xlim(self.x_lim)  # fix the x axis
        self.ax.set_ylim(self.y_lim)  # fix the y axis
        self.ax.set_xlabel(self.x_label)
        self.ax.set_ylabel(self.y_label)
        self.ax.set_title(self.title)

    def get_data(self):
        xlsx = openpyxl.load_workbook(f"{self.file_location}/{self.data_file_name}")
        sheet = xlsx[str(self.sheet_name)]
        max_row = sheet.max_row

        for row in sheet.iter_cols(min_col=1, max_col=1, min_row=2, max_row=max_row, values_only=True):
            self.x_data = list(row)
        for row in sheet.iter_cols(min_col=2, max_col=2, min_row=2, max_row=max_row, values_only=True):
            self.y_data = list(row)

        self.x_label = sheet['A1'].value
        self.y_label = sheet['B1'].value
        self.title = self.sheet_name

        last_time = self.x_data[-1]
        min_y = min(self.y_data)
        max_y = max(self.y_data)
        self.x_lim = [0, last_time]
        self.y_lim = [min_y, max_y]


if __name__ == '__main__':
    my_graph_ann_object = GraphAnnimation()
    my_graph_ann_object.file_location = str(
        input("Enter where excel workbook is located (paste the absolute path here): "))
    my_graph_ann_object.data_file_name = str(
        input("Enter workbook name: "))
    my_graph_ann_object.sheet_name = str(
        input("Enter sheet name in workbook: "))
    try:
        current_time = datetime.datetime.now()
        movie_directory = f'movies_{current_time.hour}_{current_time.minute}'
        path_to_movie = os.path.join(my_graph_ann_object.file_location, movie_directory)
        os.mkdir(path_to_movie)
        print("Creating Movie, please wait...")
        anim = animation.FuncAnimation(my_graph_ann_object.fig, my_graph_ann_object.animate, frames=200, interval=1,
                                       blit=False)
        anim.save(f'{path_to_movie}/{my_graph_ann_object.sheet_name}.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
        print("Movie created!")
        plt.show()
    except FileNotFoundError:
        print("Could not find file. Check that the provided path and file name are correct.")
