import pandas as pd
import os
import glob


class MultiCsvToXlsx:
    def __init__(self):
        self.csv_file_location = input("Enter where csv files are located (paste the absolute path here): ")
        self.name_of_xlsx_file = str(input("Enter name of output xlsx file: ")+'.xlsx')
        self.xlsx_writer = pd.ExcelWriter(self.csv_file_location+'/'+self.name_of_xlsx_file, engine='xlsxwriter')

    def multi_csv_to_xlsx_converter(self):
        print(self.xlsx_writer.path)
        csv_file_names = [os.path.basename(x) for x in glob.glob(self.csv_file_location+'/*.csv')]
        csv_files_location = glob.glob(self.csv_file_location+'/*.csv')
        for csv_file in range(0, len(csv_files_location)):
            df = pd.read_csv(csv_files_location[csv_file], header=None)
            df.to_excel(self.xlsx_writer, sheet_name=str(df.iloc[0, len(df.columns)-1])+'_'+str(csv_file), index=False, header=False)
        self.xlsx_writer.save()


if __name__ == '__main__':
    csv_to_xlsx_converter = MultiCsvToXlsx()
    csv_to_xlsx_converter.multi_csv_to_xlsx_converter()

# /home/dnightingale/PegasusVnV/cases/dn_testing/joe_demos/r_theta/3D_MPS_Case/runs/7_21_21/tabular_data
